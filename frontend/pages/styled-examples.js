import React, {Component} from 'react'
import styled from 'styled-components'

const MyButton = styled.button`
  background: ${props => props.colour};
  font-size: ${props => (props.huge ? '100px' : '50px')}; 
  `

class Styled extends Component {
  render () {
    return (
      <div>
        <p>EXAMPLES:</p>
        <MyButton huge colour='green'>Test</MyButton>
        <p>Page component</p>
        {this.props.children}
      </div>
    )
  }
}

export default Styled
