import React from 'react'
import UpdateThing from '../components/UpdateThing'

const Update = props => (
  <div>
    <UpdateThing id={props.query.id}/>
  </div>
)

export default Update
