import React from 'react'
import Things from '../components/Things'

const Home = props => (
  <div>
    <Things page={props.query.page || 1}/>
  </div>

)

export default Home
