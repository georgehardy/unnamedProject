import React, {Component} from 'react'
import {Query} from 'react-apollo'
import gql from 'graphql-tag'
import styled from 'styled-components'
import Thing from './Thing'
import Pagination from './Pagination'
import {perPage} from '../config'

const ALL_THINGS_QUERY = gql`
  query ALL_THINGS_QUERY($skip: Int = 0, $first: Int = ${perPage}) {
    things(first: $first, skip: $skip, orderBy: createdAt_DESC) {
      id
      title
      price
      description
      image
      largeImage
    }
  }
`

const Centre = styled.div`
  text-align: center;
`

const ThingsList = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 60px;
  max-width: ${props => props.theme.maxWidth};
  margin: 0 auto;
`

class Things extends Component {
  render () {
    return (
      <Centre>
        <Pagination page={this.props.page} />
        <Query
          query={ALL_THINGS_QUERY}
          variables={{
            skip: this.props.page * perPage - perPage
          }}
        >
          {({data, error, loading}) => {
            if (loading) return <p>Loading</p>
            if (error) return <p>Error: {error.message}</p>
            return <ThingsList>
              {data.things.map(thing => <Thing thing={thing} key={thing.id}/>
              )}
            </ThingsList>
          }}
        </Query>
        <Pagination page={this.props.page} />
      </Centre>
    )
  }
}

export default Things
export {ALL_THINGS_QUERY}
