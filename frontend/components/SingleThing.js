import React, {Component} from 'react'
import gql from 'graphql-tag'
import {Query} from 'react-apollo'
import Error from './ErrorMessage'
import styled from 'styled-components'
import Head from 'next/head'
import Item from './styles/ItemStyles'

const SingleThingStyles = styled.div`
  max-width: 1200px;
  margin: 2rem auto;
  box-shadow: ${props => props.theme.bs};
  display: grid;
  grid-auto-columns: 1fr;
  grid-auto-flow: column;
  min-height: 800px;
  img {
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
  .details {
    margin: 3rem;
    font-size: 2rem;
  }
`

const SINGLE_THING_QUERY = gql`
  query SINGLE_THING_QUERY($id: ID!) {
    thing(where: {id: $id}) {
      id
      title
      description
      largeImage
    }
  }
`

class SingleThing extends Component {
  render () {
    return (
      <Query
        query={SINGLE_THING_QUERY}
        variables={{id: this.props.id}}
      >
        {({error, loading, data}) => {
          if (error) return <Error error={error} />
          if (loading) return <p>Loading</p>
          if (!data.thing) return <p>Thing does not exist</p>
          const thing = data.thing
          return (
            <SingleThingStyles>
              <Head>
                <title>Library of Things | {thing.title}</title>
              </Head>
              <img src={thing.largeImage} alt={thing.title} />
              <div className="details">
                <h2>Viewing {thing.title}</h2>
                <p>{thing.description}</p>
              </div>
            </SingleThingStyles>
          )
        }}
      </Query>
    )
  }
}

export default SingleThing
