import React, {PureComponent} from 'react'
import Link from 'next/link'
import PropTypes from 'prop-types'
import Title from './styles/Title'
import ItemStyles from './styles/ItemStyles'
import PriceTag from './styles/PriceTag'
import formatMoney from '../lib/formatMoney'
import Item from './styles/ItemStyles'
import DeleteThing from './DeleteThing'

class Thing extends PureComponent {
  render () {
    const {thing} = this.props
    return (
      <ItemStyles>
        {thing.image && <img src={thing.image} alt={thing.title} />}
        <Title>
          <Link href={{
            pathname: '/thing',
            query: {id: thing.id}
          }}
          >
            <a>{thing.title}</a>
          </Link>
        </Title>
        <PriceTag>{formatMoney(thing.price)}</PriceTag>
        <p>{thing.description}</p>
        <div className="buttonList">
          <Link
            href={{
              pathname: 'update',
              query: {id: thing.id}
            }}>
            <a>Edit</a>
          </Link>
          <button>Add to Cart</button>
          <DeleteThing id={thing.id}>Delete</DeleteThing>
        </div>
      </ItemStyles>
    )
  }
}

Thing.propTypes = {
  thing: PropTypes.object.isRequired
}

export default Thing
