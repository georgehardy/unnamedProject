import React from 'react'
import gql from 'graphql-tag'
import {Query} from 'react-apollo'
import Head from 'next/head'
import Link from 'next/link'
import PaginationStyles from './styles/PaginationStyles'
import {perPage} from '../config'

const PAGINATION_QUERY = gql`
  query PAGINATION_QUERY {
    thingsConnection {
      aggregate {
        count
      }
    }
  }
`
const Pagination = props => (

  <Query query={PAGINATION_QUERY}>
    {({data, loading, error}) => {
      if (loading) return <p>Loading</p>
      const count = data.thingsConnection.aggregate.count
      const page = Number(props.page)
      const pages = Math.ceil(count / perPage)
      return (
        <PaginationStyles>
          <Head>
            <title>Library of Things | Page {page} of {pages}</title>
          </Head>
          <Link
            prefetch
            href={{
              pathname: 'things',
              query: {page: page - 1}
            }}>
            <a className="prev" aria-disabled={page <= 1}>Prev</a>
          </Link>
          <p>Page {page} of {pages}</p>
          <Link
            prefetch
            href={{
              pathname: 'things',
              query: {page: page + 1}
            }}>
            <a className="prev" aria-disabled={page >= pages}>Next</a>
          </Link>
        </PaginationStyles>
      )
    }}
  </Query>
)

export default Pagination
