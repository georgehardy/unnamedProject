import React, {Component} from 'react'
import {Mutation, Query} from 'react-apollo'
import gql from 'graphql-tag'
import Router from 'next/router'
import Form from './styles/Form'
import formatMoney from '../lib/formatMoney'
import Error from './ErrorMessage'

const SINGLE_THING_QUERY = gql`
  query SINGLE_THING_QUERY($id: ID!) {
    thing(where: { id: $id }) {
      id
      title
      description
      price
    }
  }
`
const UPDATE_THING_MUTATION = gql`
  mutation UPDATE_THING_MUTATION(
    $id: ID!
    $title: String
    $description: String
    $price: Int
  ) {
    updateThing(
      id: $id
      title: $title
      description: $description
      price: $price
    ) {
      id
      title
      description
      price
    }
  }
`

class UpdateThing extends Component {
  state = {}

  handleChange = e => {
    const {name, type, value} = e.target
    const val = type === 'number' ? parseFloat(value) : value
    this.setState({[name]: val})
  }

  updateThing = async (e, updateThingMutation) => {
    e.preventDefault()
    console.log('updating', this.props.id)
    console.log(this.state)
    const res = await updateThingMutation({
      variables: {
        id: this.props.id,
        ...this.state
      }
    })
    console.log('updated')
  }

  render () {
    return (
      <Query
        query={SINGLE_THING_QUERY}
        variables={{
          id: this.props.id
        }}
      >
        {({data, loading}) => {
          if (loading) return <p>Loading..</p>
          if (!data.thing) return <p>Thing not found</p>
          return (
            <Mutation mutation={UPDATE_THING_MUTATION} variables={this.state}>
              {(updateThing, {loading, error}) => (
                <Form onSubmit={e => this.updateThing(e, updateThing)}>
                  <Error error={error} />
                  <fieldset disabled={loading} aria-busy={loading}>
                    <label htmlFor="title">
                    Title
                      <input
                        type="text"
                        id="title"
                        name="title"
                        placeholder="Title"
                        required
                        defaultValue={data.thing.title}
                        onChange={this.handleChange}
                      />
                    </label>
                    <label htmlFor="description">
                    Description
                      <textarea
                        id="description"
                        name="description"
                        placeholder="Description"
                        required
                        defaultValue={data.thing.description}
                        onChange={this.handleChange}
                      />
                    </label>
                    <label htmlFor="price">
                    Price
                      <input
                        type="number"
                        id="price"
                        name="price"
                        placeholder="Price"
                        required
                        defaultValue={data.thing.price}
                        onChange={this.handleChange}
                      />
                    </label>
                  </fieldset>
                  <button type="submit">Sav{loading ? 'ing' : 'e'}</button>
                </Form>
              )}
            </Mutation>
          )
        }}
      </Query>
    )
  }
}

export default UpdateThing
export {UPDATE_THING_MUTATION}
