import React, {Component} from 'react'
import {Mutation} from 'react-apollo'
import gql from 'graphql-tag'
import Form from './styles/Form'
import formatMoney from '../lib/formatMoney'
import Error from './ErrorMessage'
import Router from 'next/router'

const CREATE_THING_MUTATION = gql`
  mutation CREATE_THING_MUTATION(
    $title: String!
    $description: String!
    $price: Int!
    $image: String
    $largeImage: String
  ) {
    createThing(
      title: $title
      description: $description
      price: $price
      image: $image
      largeImage: $largeImage
    ) {
      id
    }
  }
`

class CreateThing extends Component {
  state = {
    title: '',
    description: '',
    image: '',
    largeImage: '',
    price: 0
  }

  handleChange = e => {
    const {name, type, value} = e.target
    const val = type === 'number' ? parseFloat(value) : value
    this.setState({[name]: val})
  }

  uploadFile = async e => {
    const files = e.target.files
    if (files.length) {
      const data = new FormData()
      data.append('file', files[0])
      data.append('upload_preset', 'libthings')
      const res = await fetch('https://api.cloudinary.com/v1_1/alot/image/upload', {
        method: 'POST',
        body: data
      })
      const file = await res.json()
      console.log(file)
      this.setState({
        image: file.secure_url,
        largeImage: file.eager[0].secure_url
      })
    }
  }

  render () {
    return (
      <Mutation mutation={CREATE_THING_MUTATION} variables={this.state}>
        {(createThing, {loading, error}) => (
          <Form onSubmit={async e => {
            e.preventDefault()
            const res = await createThing()
            console.log(res)
            Router.push({
              pathname: '/thing',
              query: {id: res.data.createThing.id}
            })
          }}
          >
            <Error error={error} />
            <fieldset disabled={loading} aria-busy={loading}>
              <label htmlFor="file">
            Image
                <input type="file" id="file" name="file" placeholder="Image" onChange={this.uploadFile}/>
              </label>
              {this.state.image && <img width="200"src={this.state.image} alt="upload preview"/>}
              <label htmlFor="title">
            Title
                <input type="text" id="title" name="title" placeholder="Title" required value={this.state.title} onChange={this.handleChange}/>
              </label>
              <label htmlFor="description">
            Description
                <textarea id="description" name="description" placeholder="Description" required value={this.state.description} onChange={this.handleChange}/>
              </label>
              <label htmlFor="price">
            Price
                <input type="number" id="price" name="price" placeholder="Price" required value={this.state.price} onChange={this.handleChange}/>
              </label>
            </fieldset>
            <button type="submit">Submit</button>
          </Form>
        )}
      </Mutation>
    )
  }
}

export default CreateThing
export {CREATE_THING_MUTATION}
