import React, {Component} from 'react'
import {Mutation} from 'react-apollo'
import gql from 'graphql-tag'
import {ALL_THINGS_QUERY} from './Things'

const DELETE_THING_MUTATION = gql`
  mutation DELETE_THING_MUTATION($id: ID!) {
    deleteThing(id: $id) {
      id
    }
  }
`

class DeleteThing extends Component {
  update = (cache, payload) => {
    const data = cache.readQuery({query: ALL_THINGS_QUERY})
    console.log(data, payload)
    data.things = data.things.filter(things => things.id !== payload.data.deleteThing.id)
    cache.writeQuery({query: ALL_THINGS_QUERY, data})
  }

  render () {
    return (
      <Mutation
        mutation={DELETE_THING_MUTATION}
        variables={{id: this.props.id}}
        update={this.update}
      >
        {(deleteThing, {error}) => (
          <button onClick={() => {
            if (confirm('Are you sure?')) {
              deleteThing()
            }
          }}>
            {this.props.children}
          </button>
        )}
      </Mutation>
    )
  }
}

export default DeleteThing
