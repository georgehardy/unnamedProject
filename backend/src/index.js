const cookieParser = require('cookie-parser')
require('dotenv').config()
const createServer = require('./createServer')
const db = require('./db')
const jwt = require('jsonwebtoken')

const server = createServer()

server.express.use(cookieParser())

server.express.use((req, res, next) => {
  const {token} = req.cookies
  if (token) {
    const {userId} = jwt.verify(token, process.env.APP_SECRET)
    req.userId = userId
  }
  next()
})

server.start(
  {
    cors: {
      credentials: true,
      origin: process.env.FRONTEND_URL
    }
  },
  details => {
    console.log(`Server running on http://localhost:${details.port}`)
  }
)
