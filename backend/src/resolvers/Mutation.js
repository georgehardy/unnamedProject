const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {randomBytes} = require('crypto')
const {promisify} = require('util')

const Mutations = {
  async createThing (parent, args, ctx, info) {
    // todo check login

    const thing = await ctx.db.mutation.createThing(
      {
        data: {
          ...args
        }
      },
      info
    )

    return thing
  },
  updateThing (parent, args, ctx, info) {
    // take a copy of the updates
    const updates = {...args}
    // remove id from updates
    delete updates.id
    // run the update
    return ctx.db.mutation.updateThing(
      {
        data: updates,
        where: {
          id: args.id
        }
      },
      info
    )
  },
  async deleteThing (parent, args, ctx, info) {
    const where = {id: args.id}
    // find item
    const thing = await ctx.db.query.thing({where}, `{id title}`)
    // check permissions
    // delete
    return ctx.db.mutation.deleteThing({where}, info)
  },
  async signup (parent, args, ctx, info) {
    args.email = args.email.toLowerCase()
    const password = await bcrypt.hash(args.password, 10)
    const user = await ctx.db.mutation.createUser({
      data: {
        ...args,
        password,
        permissions: {set: ['USER']}
      }
    },
    info
    )
    const token = jwt.sign({userId: user.id}, process.env.APP_SECRET)
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365
    })
    return user
  },
  async signin (parent, {email, password}, ctx, info) {
    // check for user
    const user = await ctx.db.query.user({where: {email}})
    if (!user) {
      throw new Error('No user found with that email.')
    }
    // check password
    const valid = await bcrypt.compare(password, user.password)
    if (!valid) {
      throw new Error('Wrong password.')
    }
    // generate token
    const token = jwt.sign({userId: user.id}, process.env.APP_SECRET)
    // set cookie
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365
    })
    // return user
    return user
  },
  signout (parent, args, ctx, info) {
    ctx.response.clearCookie('token')
    return {message: 'Signed out.'}
  },
  async requestReset (parent, {email}, ctx, info) {
    // check if user exists
    const user = await ctx.db.query.user({where: {email: email}})
    if (!user) {
      throw new Error('No user found with that email.')
    }
    // set a token + expiry
    const resetToken = (await promisify(randomBytes)(20)).toString('hex')
    const resetTokenExpiry = Date.now() + 3600000
    const res = await ctx.db.mutation.updateUser({
      where: {email},
      data: {resetToken, resetTokenExpiry}
    })
    return {message: 'Password reset requested'}
    // email reset token
  },
  async resetPassword (parent, args, ctx, info) {
    if (args.password !== args.confirmPassword) {
      throw new Error(`Passwords don't match`)
    }
    const [user] = await ctx.db.query.users({
      where: {
        resetToken: args.resetToken,
        resetTokenExpiry_gte: Date.now() - 3600000
      }
    })
    if (!user) {
      throw new Error('Invalid token or token is expired.')
    }

    const password = await bcrypt.hash(args.password, 10)
    const updatedUser = await ctx.db.mutation.updateUser({
      where: {email: user.email},
      data: {
        password,
        resetToken: null,
        resetTokenExpiry: null
      }
    })
    const token = jwt.sign({userId: updatedUser.id}, process.env.APP_SECRET)
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365
    })
    return updatedUser
  }
}

module.exports = Mutations
