const {forwardTo} = require('prisma-binding')

const Query = {
  things: forwardTo('db'),
  thing: forwardTo('db'),
  thingsConnection: forwardTo('db'),
  me (parents, args, ctx, info) {
    if (!ctx.request.userId) {
      return null
    }
    return ctx.db.query.user({
      where: {id: ctx.request.userId}
    }, info)
  }
  // async things (parent, args, ctx, info) {
  //   const things = await ctx.db.query.things()
  //   return things
  // }
}

module.exports = Query
